package com.paytm.refundAutomationSuite.Refund;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.assertj.core.api.Assertions;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.paytm.automationCore.clients.ApplicationContextAccessor;
import com.paytm.automationCore.clients.PaytmCartClient;
import com.paytm.automationCore.crypto.ChecksumGenerator;
import com.paytm.automationCore.riverViewModel.DataObject;
import com.paytm.automationCore.riverViewModel.Driver;
import com.paytm.automationCore.riverViewModel.enums.Module;
import com.paytm.automationCore.riverViewModel.enums.OrderData;
import com.paytm.automationCore.steps.CartSteps;
import com.paytm.automationCore.steps.FFSteps;
import com.paytm.automationCore.steps.WalletSteps;
import com.paytm.automationCore.utils.ExcelWrapper;
import com.paytm.cart.model.type.CartCheckoutResponse;
import com.paytm.cart.model.type.FastForwardPGRequest;
import com.paytm.cart.model.type.GetAllSsoTokenRequest;
import com.paytm.fulfillment.responseList.ResponseObject;
import com.paytm.fulfilment.stateTransition.Graph;
import com.paytm.fulfilment.steps.FFStates;
//import com.paytm.pg.merchant.CheckSumServiceHelper;
import com.paytm.refundAutomationSuite.testBase.TestBase;

@Test(groups = "RequestRefundTest")
public class RequestRefundTest extends TestBase {

	@Autowired
	FFSteps ffSteps;
	@Autowired
	WalletSteps walletSteps;
	@Autowired
	PaytmCartClient cartClient;

	@DataProvider(parallel=true)

	public Object[][] testDataProvider() {
		int i = 0;

		ExcelWrapper excelIntegration = ExcelWrapper.getInstance();
		excelIntegration.initializeExcel(System.getProperty("user.dir")+"/src/test/resources/TestData.xlsx");
		HashMap<Integer, List> map = excelIntegration.getSheetData("Dashboard");
		System.out.println(map.entrySet().toString());
		Object[][] obj = new Object[map.size() - 1][3];
		Iterator entriesIterator = map.entrySet().iterator();
		entriesIterator.next();
		while (entriesIterator.hasNext()) {
			Map.Entry mapping = (Map.Entry) entriesIterator.next();

			List list = (List) mapping.getValue();
			for (int j = 0; j < 3; j++) {
				obj[i][j] = list.get(j);
			}
			i++;

		}
		return obj;

	}

	@Test(dataProvider = "testDataProvider")
	public void test1(Object row, Object startingMod, Object endingMod) {
		
		System.out.println("***************************************Starting the test"+row+"*******************************");
		Driver driver = new Driver();
		DataObject dobj = driver.driveUsTo(Module.valueOf(startingMod.toString()), Module.valueOf(endingMod.toString()),
				Integer.parseInt(row.toString()));
		
		Assertions.assertThat(dobj.responseObj.getRefundResposne.getRefunds().get(0).getPgAmount()).isNotNull()
				.isEqualTo(Long.parseLong(dobj.dataMap.get(OrderData.TxnAmount.toString())));
	}
	
	
	
	
	
	
	//***********These test cases below will be removed in coming time**********************

	/*@Test
	public void simpleRequestRefund() throws Exception {
		String mobile = dg.getPaytmUserMobile();
		String ssoToken = "b917d340-f615-4d95-8d20-a89417cd8e79";// authSteps.getSsoToken("5656566364",
																	// "b917d340-f615-4d95-8d20-a89417cd8e79");
		System.out.println("Mobile: " + mobile + "   ssoToken: " + ssoToken);
		String cartId = cartSteps.initializeCart(ssoToken);
		cartSteps.flushCart(ssoToken, cartId);
		cartSteps.addProductInCart(ssoToken, cartId, itemId, 1, new BigDecimal(10));
		// cartSteps.applyPromo(ssoToken, cartId, PROMO_CODE);
		cartSteps.setAddress(ssoToken, cartId, dg.getValidIndiaMobileNo(), dg.getValidAddress1(), dg.getValidAddress2(),
				dg.getValidCity(), dg.getRandomIndianState(), dg.getValidCountryName(), "110001");
		System.out.println("Wallet Balance is: " + walletSteps.checkBalance(ssoToken));
		walletSteps.addMoneyInWallet(new BigDecimal(4000), ssoToken);
		System.out.println("Wallet Balance is: " + walletSteps.checkBalance(ssoToken));
		String orderId = cartSteps.checkOut(ssoToken, cartId);
		System.out.println("Wallet Balance is: " + walletSteps.checkBalance(ssoToken));
		
		  ResponseObject obj = ffSteps.performStateTransition(orderId,
		  FFStates.ACKNOLEDGE_ORDER.toString(),
		  FFStates.SHIPMENT_CREATED.toString(), ssoToken);
		  ffSteps.performStateTransition(orderId, FFStates.CANCEL.toString(),
		  FFStates.CANCEL.toString(), ssoToken);
		  cartClient.requestRefund().setClient(dg.getValidClient()).setOrderId(
		  orderId).setVersion(dg.getValidVersion())
		  .setFulfillmentId(obj.getShipmentCreationResponse().getFulfillment_id
		  ()).call();
		  cartClient.getRefundDetail().setOrderId(orderId).setFullfillmentId(
		  obj.getShipmentCreationResponse().getFulfillment_id()) .call();
		 

	}


	@Test
	public void refundWithNANOrderId() throws Exception {

	}

	@Test
	public void refundWithNANFFId() throws Exception {

		String ssoToken = authSteps.getSsoToken(dg.getPaytmUserMobile(), dg.getRandomEmail());
		String cartId = cartSteps.initializeCart(ssoToken);
		cartSteps.flushCart(ssoToken, cartId);
		cartSteps.addProductInCart(ssoToken, cartId, itemId, 1, new BigDecimal(10));
		cartSteps.applyPromo(ssoToken, cartId, PROMO_CODE);
		cartSteps.setAddress(ssoToken, cartId, dg.getValidIndiaMobileNo(), dg.getValidAddress1(), dg.getValidAddress2(),
				dg.getValidCity(), dg.getRandomIndianState(), dg.getValidCountryName(), "110001");
		String orderId = cartSteps.checkOut(ssoToken, cartId);
		ResponseObject obj = ffSteps.performStateTransition(orderId, FFStates.ACKNOLEDGE_ORDER.toString(),
				FFStates.SHIPMENT_CREATED.toString(), ssoToken);
		cartClient.requestRefund().setClient(dg.getValidClient()).setOrderId(orderId).setVersion(dg.getValidVersion())
				.setFulfillmentId(dg.getRandomAlphaNumericString(8)).call();
	}

	@Test
	public void requestRefundWithOutOrderId() throws Exception {

		String ssoToken = authSteps.getSsoToken(dg.getPaytmUserMobile(), dg.getRandomEmail());
		String cartId = cartSteps.initializeCart(ssoToken);
		cartSteps.flushCart(ssoToken, cartId);
		cartSteps.addProductInCart(ssoToken, cartId, itemId, 1, new BigDecimal(10));
		cartSteps.applyPromo(ssoToken, cartId, PROMO_CODE);
		cartSteps.setAddress(ssoToken, cartId, dg.getValidIndiaMobileNo(), dg.getValidAddress1(), dg.getValidAddress2(),
				dg.getValidCity(), dg.getRandomIndianState(), dg.getValidCountryName(), "110001");
		String orderId = cartSteps.checkOut(ssoToken, cartId);
		ResponseObject obj = ffSteps.performStateTransition(orderId, FFStates.ACKNOLEDGE_ORDER.toString(),
				FFStates.SHIPMENT_CREATED.toString(), ssoToken);
		cartClient.requestRefund().setClient(dg.getValidClient()).setVersion(dg.getValidVersion())
				.setFulfillmentId(obj.getShipmentCreationResponse().getFulfillment_id()).call();

	}

	@Test
	public void RequestRefundWithOutFfId() throws Exception {

		String ssoToken = authSteps.getSsoToken(dg.getPaytmUserMobile(), dg.getRandomEmail());
		String cartId = cartSteps.initializeCart(ssoToken);
		cartSteps.flushCart(ssoToken, cartId);
		cartSteps.addProductInCart(ssoToken, cartId, itemId, 1, new BigDecimal(10));
		cartSteps.applyPromo(ssoToken, cartId, PROMO_CODE);
		cartSteps.setAddress(ssoToken, cartId, dg.getValidIndiaMobileNo(), dg.getValidAddress1(), dg.getValidAddress2(),
				dg.getValidCity(), dg.getRandomIndianState(), dg.getValidCountryName(), "110001");
		String orderId = cartSteps.checkOut(ssoToken, cartId);
		ResponseObject obj = ffSteps.performStateTransition(orderId, FFStates.ACKNOLEDGE_ORDER.toString(),
				FFStates.SHIPMENT_CREATED.toString(), ssoToken);
		cartClient.requestRefund().setClient(dg.getValidClient()).setOrderId(orderId).setVersion(dg.getValidVersion())
				.call();

	}

	@Test
	public void RequestRefundWithInvalidOrderId() throws Exception { // OrderId
																		// is
																		// still
																		// a
																		// number
		String ssoToken = authSteps.getSsoToken(dg.getPaytmUserMobile(), dg.getRandomEmail());
		String cartId = cartSteps.initializeCart(ssoToken);
		cartSteps.flushCart(ssoToken, cartId);
		cartSteps.addProductInCart(ssoToken, cartId, itemId, 1, new BigDecimal(10));
		cartSteps.applyPromo(ssoToken, cartId, PROMO_CODE);
		cartSteps.setAddress(ssoToken, cartId, dg.getValidIndiaMobileNo(), dg.getValidAddress1(), dg.getValidAddress2(),
				dg.getValidCity(), dg.getRandomIndianState(), dg.getValidCountryName(), "110001");
		String orderId = cartSteps.checkOut(ssoToken, cartId);
		ResponseObject obj = ffSteps.performStateTransition(orderId, FFStates.ACKNOLEDGE_ORDER.toString(),
				FFStates.SHIPMENT_CREATED.toString(), ssoToken);
		cartClient.requestRefund().setClient(dg.getValidClient()).setOrderId(dg.getAnyRandomNumeric(8))
				.setVersion(dg.getValidVersion())
				.setFulfillmentId(obj.getShipmentCreationResponse().getFulfillment_id()).call();

	}

	@Test
	public void RequestRefundWithInvalidFulfillmentId() throws Exception {

		String ssoToken = authSteps.getSsoToken(dg.getPaytmUserMobile(), dg.getRandomEmail());
		String cartId = cartSteps.initializeCart(ssoToken);
		cartSteps.flushCart(ssoToken, cartId);
		cartSteps.addProductInCart(ssoToken, cartId, itemId, 1, new BigDecimal(10));
		cartSteps.applyPromo(ssoToken, cartId, PROMO_CODE);
		cartSteps.setAddress(ssoToken, cartId, dg.getValidIndiaMobileNo(), dg.getValidAddress1(), dg.getValidAddress2(),
				dg.getValidCity(), dg.getRandomIndianState(), dg.getValidCountryName(), "110001");
		String orderId = cartSteps.checkOut(ssoToken, cartId);
		ResponseObject obj = ffSteps.performStateTransition(orderId, FFStates.ACKNOLEDGE_ORDER.toString(),
				FFStates.SHIPMENT_CREATED.toString(), ssoToken);
		cartClient.requestRefund().setClient(dg.getValidClient()).setOrderId(orderId).setVersion(dg.getValidVersion())
				.setFulfillmentId(dg.getAnyRandomNumeric(8)).call();

	}

	@Test
	public void requestRefundWhenRefundAlreadyProcessed() throws Exception {

		
		  String ssoToken=authSteps.getSsoToken(dg.getPaytmUserMobile(),
		  dg.getRandomEmail()); String
		  cartId=cartSteps.initializeCart(ssoToken);
		  cartSteps.flushCart(ssoToken, cartId);
		  cartSteps.addProductInCart(ssoToken, cartId, itemId, 1, new
		  BigDecimal(10)); cartSteps.applyPromo(ssoToken, cartId, PROMO_CODE);
		  cartSteps.setAddress(ssoToken, cartId, dg.getValidIndiaMobileNo(),
		  dg.getValidAddress1(), dg.getValidAddress2(), dg.getValidCity(),
		  dg.getRandomIndianState(), dg.getValidCountryName(), "110001");
		  String orderId=cartSteps.checkOut(ssoToken, cartId); ResponseObject
		  obj=ffSteps.performStateTransition(orderId,
		  FFStates.ACKNOLEDGE_ORDER.toString(),
		  FFStates.SHIPMENT_CREATED.toString(), ssoToken);
		 
		// ResponseObject obj = ffSteps.performStateTransition(itemId, null,
		// null, FFStates.SIPMENT_CREATED.toString(), null);
		
		  String orderId2=obj.getAcknowledgeOrderResponse().getOrder_id();
		  cartClient.requestRefund().setClient(dg.getValidClient()).setOrderId(
		  orderId).setVersion(dg.getValidVersion()).setFulfillmentId(obj.
		  getShipmentCreationResponse().getFulfillment_id()).call();
		 
		System.out.println(ffSteps.performStateTransition(itemId, null, FFStates.SHIPMENT_CREATED.toString())
				.getShipmentCreationResponse().getFulfillment_id());
	}

	@Test
	public void requestRefundWhenRefundInProgress() throws Exception {

		
		  String ssoToken=authSteps.getSsoToken(dg.getPaytmUserMobile(),
		  dg.getRandomEmail()); String
		  cartId=cartSteps.initializeCart(ssoToken);
		  cartSteps.flushCart(ssoToken, cartId);
		  cartSteps.addProductInCart(ssoToken, cartId, itemId, 1, new
		  BigDecimal(10)); cartSteps.applyPromo(ssoToken, cartId, PROMO_CODE);
		  cartSteps.setAddress(ssoToken, cartId, dg.getValidIndiaMobileNo(),
		  dg.getValidAddress1(), dg.getValidAddress2(), dg.getValidCity(),
		  dg.getRandomIndianState(), dg.getValidCountryName(), "110001");
		  String orderId=cartSteps.checkOut(ssoToken, cartId); ResponseObject
		  obj=ffSteps.performStateTransition(orderId,
		  FFStates.ACKNOLEDGE_ORDER.toString(),
		  FFStates.SHIPMENT_CREATED.toString(), ssoToken);
		 
		// ResponseObject obj = ffSteps.performStateTransition(itemId, null,
		// null, FFStates.SIPMENT_CREATED.toString(), null);
		
		  String orderId2=obj.getAcknowledgeOrderResponse().getOrder_id();
		  cartClient.requestRefund().setClient(dg.getValidClient()).setOrderId(
		  orderId).setVersion(dg.getValidVersion()).setFulfillmentId(obj.
		  getShipmentCreationResponse().getFulfillment_id()).call();
		 
		System.out.println(ffSteps.performStateTransition(itemId, null, FFStates.SHIPMENT_CREATED.toString())
				.getShipmentCreationResponse().getFulfillment_id());
	}

	@Test
	public void requestRefundWhenRefundAlreadyCancelled() throws Exception {

		
		  String ssoToken=authSteps.getSsoToken(dg.getPaytmUserMobile(),
		  dg.getRandomEmail()); String
		  cartId=cartSteps.initializeCart(ssoToken);
		  cartSteps.flushCart(ssoToken, cartId);
		  cartSteps.addProductInCart(ssoToken, cartId, itemId, 1, new
		  BigDecimal(10)); cartSteps.applyPromo(ssoToken, cartId, PROMO_CODE);
		  cartSteps.setAddress(ssoToken, cartId, dg.getValidIndiaMobileNo(),
		  dg.getValidAddress1(), dg.getValidAddress2(), dg.getValidCity(),
		  dg.getRandomIndianState(), dg.getValidCountryName(), "110001");
		  String orderId=cartSteps.checkOut(ssoToken, cartId); ResponseObject
		  obj=ffSteps.performStateTransition(orderId,
		  FFStates.ACKNOLEDGE_ORDER.toString(),
		  FFStates.SHIPMENT_CREATED.toString(), ssoToken);
		 
		// ResponseObject obj = ffSteps.performStateTransition(itemId, null,
		// null, FFStates.SIPMENT_CREATED.toString(), null);
		
		  String orderId2=obj.getAcknowledgeOrderResponse().getOrder_id();
		  cartClient.requestRefund().setClient(dg.getValidClient()).setOrderId(
		  orderId).setVersion(dg.getValidVersion()).setFulfillmentId(obj.
		  getShipmentCreationResponse().getFulfillment_id()).call();
		 
		System.out.println(ffSteps.performStateTransition(itemId, null, FFStates.SHIPMENT_CREATED.toString())
				.getShipmentCreationResponse().getFulfillment_id());
	}

	@Test
	public void requestRefundWhenFFIdNotGenerated() {

	}

	@Test
	public void requestpartialRefundofItem1() {

	}

	@Test
	public void requestRefundofItem2whenItem1AlreadyRefunded() {

	}

	@Test
	public void requestRefundforDigitalProduct() {

	}

	@Test
	public void requestRefundWhenRefundValueisTooSmall() {// Refund should
															// process
															// Immediately

	}

	@Test
	public void requestRefundInCaseOfFailedOrder() {

	}

	@Test
	public void requestRefundInCaseOfPendingOrder() { // Status=1

	}

	@Test
	public void requestRefundWithCancellationCharges() {

	}

	@Test
	public void requestRefundForNonRefundableOrder() {

	}

	@Test(enabled=false)
	public void checksumTest() throws Exception {
		System.out.println(System.getProperty("user.dir"));
		ChecksumGenerator cg = new ChecksumGenerator();
		cg.setMerchantkey("hClCeBrH76t8%IZc").setCurrency("INR").setCustomerId("1107217021").setDeviceId("5656566364")
				.setIndustryType("Retail").setMId("Mplace59064363019056").setOrderId("4294986260").setPaymentMode("PPI")
				.setReqType("CLW_APP_PAY").setSsoToken("b66920e0-60de-4d1d-b103-bdb7bfc0b1f2").setTxnAmount("4000");
		String checksum = cg.getCheckSum();
		System.out.println("Checksum Value is:  " + checksum);
		FastForwardPGRequest ff = new FastForwardPGRequest(cg);
		ff.setAuthMode("USRPWD").setChannel("WEB").setEMAIL("neha.chauhan@paytm.com").setMSISDN("5656566364")
				.setTokenType("OAUTH").setCHECKSUMHASH(checksum);
		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(ff);
		ff.setJsonData(json);
		ff.call();

	}

	

	@Test
	public void completeRefund() throws Exception {
		// Generate SSo Token

		System.out.println("***************Generating ssoToken**************");
		String ssoToken = authSteps.getSsoToken(dg.getPaytmUserMobile(), dg.getRandomEmail());

		System.out.println("***************Generating Order**************");
		// Generate Order
		String cartId = cartSteps.initializeCart(ssoToken);
		cartSteps.flushCart(ssoToken, cartId);
		cartSteps.addProductInCart(ssoToken, cartId, dg.getPhysicalProductId(), 1, new BigDecimal(10));
		walletSteps.addMoneyInWallet(new BigDecimal(4000), ssoToken);
		cartSteps.setAddress(ssoToken, cartId, dg.getRandomAlphaNumericString(10), "Test", "Test", dg.getValidCity(),
				dg.getRandomIndianState(), dg.getValidCountryName(), "110001");
		CartCheckoutResponse response = cartSteps.checkoutWithOutAuthorization(ssoToken, cartId, "web", "1",
				dg.getValidVersion());
		String orderId = response.getOrderId();
		String customerId = response.getCustId();
		String Mid = response.getMid();
		BigDecimal TxnAmount = response.getTxnAmount();

		System.out.println("***************Generating WalletScope ssoToken**************");
		// Get Wallet scope ssoToken
		GetAllSsoTokenRequest request = cartClient.getAllSsoToken().setSsoToken(ssoToken);
		Map<String, String> headerMap = new HashMap<String, String>();
		headerMap.put("Authorization", "Basic dGVzdGNsaWVudDo5MzhkZWJjYS02NWQyLTQ1NzEtYTEwZC0xMzQwNTNmNzhkNTE=");
		request.setHeaderMap(headerMap);
		String walletToken = request.call().getWalletSsoToken();

		// Generate Checksum
		System.out.println("***************Generating Checksum**************");
		ChecksumGenerator cg = new ChecksumGenerator();
		cg.setMerchantkey("hClCeBrH76t8%IZc").setCurrency("INR").setCustomerId(customerId).setDeviceId("5656566364")
				.setIndustryType("Retail").setMId(Mid).setOrderId(orderId).setPaymentMode("PPI")
				.setReqType("CLW_APP_PAY").setSsoToken(walletToken).setTxnAmount(TxnAmount.toString());
		String checksum = cg.getCheckSum();

		// PG fastForwardCheckout
		System.out.println("***************PG fast forwardChekout**************");
		FastForwardPGRequest ff = cartClient.fastForwardCheckOut(cg);
		ff.setAuthMode("USRPWD").setChannel("WEB").setEMAIL("neha.chauhan@paytm.com").setMSISDN("5656566364")
				.setTokenType("OAUTH").setCHECKSUMHASH(checksum);
		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(ff);
		ff.setJsonData(json);
		try {
			ff.call();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}

		// PaymentAuthorization
		System.out.println("***************Authorizing payment**************");
		cartSteps.authorize(response);

		// PaymentStatusCHeck
		System.out.println("***************Payment Status Check**************");
		cartClient.getPaymentStatus().setId(orderId).call();

		// StateTransition and cancellations
		System.out.println("***************Changing state **************");
		ffSteps.performStateTransition(orderId, FFStates.ACKNOLEDGE_ORDER.toString(),
				FFStates.READY_TO_SHIP.toString(), ssoToken);
		System.out.println("***************Cancelling Order**************");
		ffSteps.performStateTransition(orderId, FFStates.READY_TO_SHIP.toString(), FFStates.CANCEL.toString(),
				ssoToken);

	}





*/}
