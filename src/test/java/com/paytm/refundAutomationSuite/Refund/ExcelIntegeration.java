package com.paytm.refundAutomationSuite.Refund;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import com.paytm.automationCore.utils.ExcelReader;

public class ExcelIntegeration {
	
	HashMap<String, LinkedHashMap<Integer, List>> result;
	private static ExcelIntegeration excelIntegration = new ExcelIntegeration();
	
	private ExcelIntegeration(){
		
	}
	public static ExcelIntegeration getInstance(){
		return excelIntegration;
	}
	
	public HashMap<String, LinkedHashMap<Integer, List>> initializeExcel(String path){
		
		ExcelReader reader = new ExcelReader();
		File file = new File(path);
		HashMap<String, LinkedHashMap<Integer, List>> result = reader.loadExcelLines(file);
		this.result=result;
		return this.result;
	}
	
	public Set<String> getSheetNames(){
		
		return result.keySet();
		
	}
	
	public LinkedHashMap<Integer,List> getSheetData(String sheetName){
		LinkedHashMap<Integer,List> sheetData= result.get(sheetName);
		return sheetData;
		
	}
	public ArrayList[][] getAllRowsofSheet(LinkedHashMap<Integer,List> sheetData,int cols){
		
		int rows= sheetData.size();
		getSelectedRowsofSheet(sheetData,cols,0,rows);
		
		return null;
	}
	
	public ArrayList<String>[][] getSelectedRowsofSheet(LinkedHashMap<Integer,List> sheetData,int cols,int startRow, int endRow){
		
		ArrayList<String>[][] data= new ArrayList[endRow][cols];
		for(int i=startRow;i<endRow;i++){
			for(int j=0;j<cols;j++){
				data[i][j].add(sheetData.get(i).get(j).toString());
			}
		}
		return data;
	}

}
