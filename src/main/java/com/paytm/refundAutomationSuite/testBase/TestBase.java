package com.paytm.refundAutomationSuite.testBase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;

import com.paytm.automationCore.clients.PaytmCartClient;
import com.paytm.automationCore.steps.AuthSteps;
import com.paytm.automationCore.steps.CartSteps;
import com.paytm.automationCore.utils.CartTestDataGenerator;
import com.paytm.automationCore.utils.TestDataGenerator;

@ContextConfiguration(locations = { "classpath:spring/application-context.xml" })
public class TestBase extends AbstractTestNGSpringContextTests {
	
	
	@Autowired
	protected AuthSteps authSteps;
	@Autowired
	protected CartSteps cartSteps;
	@Autowired
	protected CartTestDataGenerator dg;
	@Autowired
	protected PaytmCartClient cartClient;
	@Autowired
	protected TestDataGenerator td;
	
	@Value("${PROMO_CODE}")
	protected String PROMO_CODE;
	
	@Value("${PHYSICAL_ITEM}")
	protected String itemId;
	
	@Value("${DIGITAL_ITEM}")
	protected String digitalItemId;

}
