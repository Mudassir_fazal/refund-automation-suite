#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.testBase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;

import ${groupId}.automationCore.clients.PaytmCartClient;
import ${groupId}.automationCore.steps.AuthSteps;
import ${groupId}.automationCore.steps.CartSteps;
import ${groupId}.automationCore.utils.CartTestDataGenerator;
import ${groupId}.automationCore.utils.TestDataGenerator;

@ContextConfiguration(locations = { "classpath:spring/application-context.xml" })
public class TestBase extends AbstractTestNGSpringContextTests {
	
	
	@Autowired
	protected AuthSteps authSteps;
	@Autowired
	protected CartSteps cartSteps;
	@Autowired
	protected CartTestDataGenerator dg;
	@Autowired
	protected PaytmCartClient cartClient;
	@Autowired
	protected TestDataGenerator td;
	
	@Value("${symbol_dollar}{PROMO_CODE}")
	protected String PROMO_CODE;
	
	@Value("${symbol_dollar}{PHYSICAL_ITEM}")
	protected String itemId;
	
	@Value("${symbol_dollar}{DIGITAL_ITEM}")
	protected String digitalItemId;

}
